package br.com.controle;

import br.com.entidade.Cliente;
import br.com.service.ClienteService;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path(value = "/cliente")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClienteController implements Serializable {
   
    @Inject
    private ClienteService clienteService;   

    @GET
    @Path("/listagem")
    public List<Cliente> listagem() {
        return clienteService.listar();
    }

    @POST
    @Path("/salvar")
    public void salvar(Cliente c) throws Exception {
        clienteService.salvar(c);
    }

    @POST
    @Path("/excluir")
    public void excluir(Cliente c) {
        clienteService.excluir(c);
    }

}
