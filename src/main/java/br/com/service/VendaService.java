package br.com.service;

import br.com.entidade.Produto;
import br.com.entidade.Venda;
import br.com.entidade.VendaItem;
import br.com.persistencia.Transacional;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author barizon
 */
@Transacional
public class VendaService extends AbstractService<Venda> implements Serializable {

    @Inject
    public EntityManager em;

    public VendaService() {
        super(Venda.class);
    }

    @Override
    public Venda salvar(Venda entidade) throws Exception {
        for (VendaItem item : entidade.getVendaItens()) {
            Produto p = em.find(Produto.class, item.getProduto().getId());
            if (!p.getPreco().equals(item.getPreco())) {
                throw new Exception("O preço do produto " + p.getNome() + " não confere com o cadastrado");
            }
            if (item.getQuantidade().compareTo(p.getEstoque()) > 0) {
                throw new Exception("Estoque do produto " + p.getNome() + " é insuficiente para venda");
            }
        }
        if (entidade.getDesconto().compareTo(entidade.getTotal()) >= 0) {
            throw new Exception("Desconto não permitido, pois ultrapassa o valor total");
        }
        entidade.calculaTotal();
        return super.salvar(entidade);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
}
