package br.com.service;


import br.com.entidade.Produto;
import br.com.persistencia.Transacional;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author barizon
 */
@Transacional
public class ProdutoService extends AbstractService<Produto> implements Serializable {
    
    @Inject
    public EntityManager em;
    
    public ProdutoService() {
        super(Produto.class);
    }
    
    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public List<Produto> produtoAutoComplete(String nome) {
        return em.createQuery("FROM Produto AS p WHERE LOWER(p.nome) "
                + "LIKE '%" + nome.toLowerCase() + "%'").getResultList();
    }
}

